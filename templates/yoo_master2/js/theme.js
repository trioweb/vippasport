/* Copyright (C) YOOtheme GmbH, http://www.gnu.org/licenses/gpl.html GNU/GPL */

jQuery(function($) {

    var config = $('html').data('config') || {};

    // Social buttons
    $('article[data-permalink]').socialButtons(config);

});




jQuery(document).ready(function($) {

    var kod = '';
    let lng;
    let lat;
    let col;
    let timeLabel;
    let saveData = {};
    let trackers;
    let client_id;
    let sessind_id = new Date().getTime() + '.' + Math.random().toString(36).substring(5);


    

     // проверка доступности выезда в городе по координатам
     if ($('#next_btn2Next').hasClass('uk-button')) {
     	document.getElementById("next_btn2Next").setAttribute("disabled","disable");
     	document.getElementById("next_btn3Next").setAttribute("disabled","disable");
     }
     

    // запрос на получения доступного времени
    $("#next_btn2Next").click(function(e) {


        col = $('.count-people').val();

        var Mapss = function geoadres(adress) {
            var resultlat = '';
            var resultlng = '';
            $.ajax({
                async: false,
                dataType: "json",
                url: 'https://maps.google.com/maps/api/geocode/json?address=' + adress + '&key=AIzaSyCLIniv_FdUi9ke2YLQGYEdo1EMDHwlpNI',
                success: function(data) {
                    for (var key in data.results) {
                        resultlat = data.results[key].geometry.location.lat;
                        resultlng = data.results[key].geometry.location.lng;
                    }
                }
            });
            return { lat: resultlat, lng: resultlng }
        }

        lat = new Mapss($('#map_search').val()).lat;
        lng = new Mapss($('#map_search').val()).lng;



        // $("#next_btn2Next").click(function(e) { 

        let startDate;
        let endDate;

        let objData = {};




        // (1)
        var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

        var xhr = new XHR();

        // (2) запрос на другой домен :)
        xhr.open('GET', '//td.dpdok.com.ua:38383/running/WebDBInt?rul_=getFreeTime&lat=' + lat + '&lng=' + lng + '&cnt=' + col, true);

        // xhr.onload = function() {
        //   alert( this.responseText );
        // }

        xhr.onerror = function() {
            alert('Ошибка ' + this.status);
        }

        xhr.send();

        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) {
                return
            }
            if (xhr.status === 200) {
                // let xml = $.parseXML(xhr.responseText);
                let xml = xhr.responseXML;
                let objMain = xml.getElementsByTagName('ns1:description')[0].innerHTML;

                localStorage.setItem("test1", objMain);

                if (objMain.length > 100) {

                    let reg = objMain.match(/\[.*\]/i);
                    let location = objMain.match(/\:\{.*?\}/i);
                    let currDate = [];
                    // console.log(location)
                    location = location[0].substring(1, location[0].length)

                    location = JSON.parse(location)

                    // получили код города	
                    kod = Number(location.kod);
                    $('#validate-area').val(Number(kod));
                    // console.log(location.kod);

                    objMain = reg[0];
                    objMain = objMain.substring(1, objMain.length - 1);
                    timeMain = objMain.split(/,| /)

                    // console.log(timeMain[0]);

                    startDate = timeMain[0]
                    endDate = timeMain[timeMain.length - 2];

                    // console.log(startDate+' '+endDate);

                    // календарь
                    if ($('html').attr('lang') == 'ru-ru') {
                        $.datetimepicker.setLocale('ru');
                    } else {
                        $.datetimepicker.setLocale('uk');
                    }

                    $('#filter-date').datetimepicker({
                        // startDate:startDate,formatDate:'Y-m-d',
                        // maxDate:startDate,formatDate:'Y-m-d',
                        validateOnBlur: false,
                        todayButton: false,
                        // minTime: '10:00',
                        // maxTime: '17:01',
                        scrollMonth: false,
                        dayOfWeekStart: 1,
                        format: 'Y-m-d',
                        // step: 30,
                        // weekends: [1, 6],
                        // allowTimes: ['10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00'],
                        defaultSelect: false,
                        closeOnDateSelect: false,
                        closeOnWithoutClick: false,
                        timepicker:false,
                        onSelectDate: function(ct, $i) {
                            $(this).find('.xdsoft_timepicker').addClass('active');
                            $("#next_btn3Next").attr('disabled', 'disabled');

                            let currTime = [];
                            for (var i = 0; i < timeMain.length; i++) {
                                if ($i.val().substring(0, 10) == timeMain[i]) {
                                    currTime.push(timeMain[i + 1].substring(0, 5));
                                    // console.log($i.val())
                                }
                            }
                            this.setOptions({
                                allowTimes: currTime,
                                format: 'Y-m-d H:i',
                                timepicker:true
                            })
                        },
                        onSelectTime: function(){

                           $("#next_btn3Next").removeAttr('disabled');
                        },
                        onGenerate: function() {
                            // $(this).find('.xdsoft_date.xdsoft_weekend')
                                // .addClass('xdsoft_disabled');
                        },
                        onShow: function() {
                            currDate = [];
                            for (var i = 0; i < timeMain.length; i++) {
                                if (i % 2 == 0) {
                                    currDate.push(timeMain[i]);
                                }

                            };
                            this.setOptions({
                                allowDates: currDate,
                                formatDate: 'Y-m-d'
                            })
                            $('#filter-date').attr('readonly', 'readonly');
						   	
                        },
                        onClose: function() {
                            $('#filter-date').removeAttr('readonly');
                        }
                    });


                }

                // console.log(lat + ":" + lng + " - " + col + " - " + location.text);

            } else {
                console.log('err', xhr.responseText);
            }

        }


    });


  



    // запрос резервирования времени /////////////////////////////
    $('#next_btn3Next').click(function(event) {

        var timetoGet = $('#filter-date').val() + ':00';

        // (1)
        var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

        var xhr = new XHR();

        // (2) запрос на другой домен :)
        xhr.open('GET', '//td.dpdok.com.ua:38383/running/WebDBInt?rul_=reservTime&dt=' + timetoGet + '&kod=' + kod + '&lat=' + lat + '&lng=' + lng + '&cnt=' + col, true);

        // xhr.onload = function() {
        //   alert( this.responseText );
        // }

        xhr.onerror = function() {
            alert('Ошибка ' + this.status);
        }

        xhr.send();

        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) {
                return
            }
            if (xhr.status === 200) {
                // let xml = $.parseXML(xhr.responseText);
                let xml = xhr.responseXML;
                let objMain = xml.getElementsByTagName('ns1:description')[0].innerHTML;
                timeLabel = objMain;

                // console.log(objMain);
                localStorage.setItem("test2", objMain);
            } else {
                console.log('err', xhr.responseText);
            }
        }

    });


    // сохранение заявки ////////////////////////////////////////

    $('#send_bnt').click(function(event) {

        if ($('#approve_check0').prop('checked')) {

            saveData.addr = $('#map_search').val();
            saveData.pers = [];
            saveData.tels = [];
            saveData.tels.push($('#contact_phone').val());

            if ($('#aditional_info').val() != '') {
                saveData.comment = $('#aditional_info').val();
            }

           

    
            saveData.www_c_id = client_id;
            saveData.www_s_id = sessind_id;


            let fioPerson = {};

            if ($('#fio1').val() != "") {
                let arrfio1 = $('#fio1').val().split(' ');
                let objfio1 = {
                    "fam": arrfio1[0],
                    "im1": arrfio1[1],
                    "im2": arrfio1[2],
                    "pasp": null
                }
                saveData.pers.push(objfio1);
            }
            if ($('#fio2').val() != "") {
                let arrfio2 = $('#fio2').val().split(' ');
                let objfio2 = {
                    "fam": arrfio2[0],
                    "im1": arrfio2[1],
                    "im2": arrfio2[2],
                    "pasp": null
                }
                saveData.pers.push(objfio2);
            }
            if ($('#fio3').val() != "") {
                let arrfio3 = $('#fio3').val().split(' ');
                let objfio3 = {
                    "fam": arrfio3[0],
                    "im1": arrfio3[1],
                    "im2": arrfio3[2],
                    "pasp": null
                }
                saveData.pers.push(objfio3);
            }
            if ($('#fio4').val() != "") {
                let arrfio4 = $('#fio4').val().split(' ');
                let objfio4 = {
                    "fam": arrfio4[0],
                    "im1": arrfio4[1],
                    "im2": arrfio4[2],
                    "pasp": null
                }
                saveData.pers.push(objfio4);
            }
            if ($('#fio5').val() != "") {
                let arrfio5 = $('#fio5').val().split(' ');
                let objfio5 = {
                    "fam": arrfio5[0],
                    "im1": arrfio5[1],
                    "im2": arrfio5[2],
                    "pasp": null
                }
                saveData.pers.push(objfio5);
            }


            // (1)
            var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

            var xhr = new XHR();

            // (2) запрос на другой домен :)
            xhr.open('GET', ' //td.dpdok.com.ua:38383/running/WebDBInt?rul_=saveRequest&idbl=' + timeLabel + '&js=' + encodeURIComponent(JSON.stringify(saveData)), true);

            // xhr.onload = function() {
            //   alert( this.responseText );
            // }

            xhr.onerror = function() {
                alert('Ошибка ' + this.status);
            }

            xhr.send();

            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) {
                    return
                }
                if (xhr.status === 200) {
                    // let xml = $.parseXML(xhr.responseText);
                    let xml = xhr.responseXML;
                    let objMain = xml.getElementsByTagName('ns1:description')[0].innerHTML;

                    // console.log(objMain);
                    localStorage.setItem("test3", objMain);
                } else {
                    console.log('err', xhr.responseText);
                }
            }

        }

        console.log(JSON.stringify(saveData));

    });

    $('.fio').on('keyup keypress', function(e) {
        var letters = " АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхХхЧчЦцШшЩщЬьЮюЯя'";
        return (letters.indexOf(String.fromCharCode(e.which)) != -1);
    });

    // транслитерация фио ///////////////////////////////////////
    $('.fio').each(function(index, el) {
        $(this).keyup(function(event) {
            let thisvalue = $(this).val();
            let pastekey = $(this).closest('.rsform-block').next('.rsform-block').find('.fio-translit');
            pastekey = pastekey.val(thisvalue);
            pastekey.liTranslit();
        });
    });

    $('.uk-accordion.accordion-second .uk-accordion-title').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('uk-active');
    });

    $('.uk-accordion.accordion-third .uk-accordion-title').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('uk-active');
    });
    $('.uk-accordion.accordion-four .uk-accordion-title').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('uk-active');
    });

    // подсчет стоимость оформления паспорта
    $('#count_people').change(function(event) {
        let count_people = Number($(this).val())+1;
            $('.price-field').text($('.o-table.o-50 > tbody > tr:last-child > td:nth-of-type('+count_people+')').text());

        // $('.price-field').text($('#hidden18').val() * $(this).val());
    });

    // подсчет стоимости мобильной выдачи
    $('#count_pasport').change(function(event) {
        let mob_count_people = Number($(this).val())+1;
            $('.mob-price-field').text($('.v-table.v-50 > tbody > tr:last-child > td:nth-of-type('+mob_count_people+')').text());

    });

    // $('.price-field').text($('#hidden18').val());

    $('.form-blue').attr('id', 'main-form');
    $('a[href ~= main-form]').attr('data-uk-smooth-scroll', '');

    // выводим результат проверки формы выездного оформления

    $('.first-form .rsform-button-next').click(function(event) {
        $('#data1').next('li').text($('#count_people').val());
        $('#data2').next('li').html('<span>' + $('#map_search').val() + '</span>');
        $('#data3').next('li').text($('#filter-date').val());
        $('#data4').next('li').text($('#fio1').val());
        $('#data5').next('li').text($('#fio_trans1').val());
        $('#data4_1').next('li').text($('#fio2').val());
        $('#data5_1').next('li').text($('#fio_trans2').val());
        $('#data4_2').next('li').text($('#fio3').val());
        $('#data5_2').next('li').text($('#fio_trans3').val());
        $('#data4_3').next('li').text($('#fio4').val());
        $('#data5_3').next('li').text($('#fio_trans4').val());
        $('#data4_4').next('li').text($('#fio5').val());
        $('#data5_4').next('li').text($('#fio_trans5').val());
        $('#data5_5').next('li').text($('.rsform-radio:checked').val());
        $('#data6').next('li').text($('#contact_phone').val());
        $('#data7').next('li').text($('#aditional_info').val());
        $('#data8').next('li').text($('.price-field').text() + 'грн.');

        if ($('#fio2').val() != '') {
            $('#data4_1, #data5_1').removeClass('uk-hidden');
        }
        if ($('#fio3').val() != '') {
            $('#data4_2, #data5_2').removeClass('uk-hidden');
        }
        if ($('#fio4').val() != '') {
            $('#data4_3, #data5_3').removeClass('uk-hidden');
        }
        if ($('#fio5').val() != '') {
            $('#data4_4, #data5_4').removeClass('uk-hidden');
        }

         // добавляем переменные session id и client id

            trackers = ga.getAll()[0];
            client_id = trackers.get('clientId');
            // console.log("www_c_id "+client_id.substr(-10,10));
            // console.log("www_s_id "+client_id.substr(0,8));
            // $('#www_C_ID, #m_www_C_ID').val(client_id.substr(-10.10));

        // добавляем номер заявки в скрытое поле
        $('#numer_id').val(localStorage.getItem("test2"));

        $('#www_C_ID').val(client_id);
        $('#www_S_ID').val(sessind_id);

    });

    // добавляем номер заказа в сообщение благодарности за заказ.
    setTimeout(function() {

    	if ( $('span').hasClass('numer-id')) {
	    	$('.numer-id').text(localStorage.getItem("test2"));
	    }

    }, 800);
    
   

    // выводим результаты проверки формы мобильной выдачи
    $('.mobile-form .rsform-button-next').click(function(event) {
        $('#mob_data1').next('li').text($('#count_pasport').val());
        $('#mob_data1_1').next('li').text($('#hidden_mob_addres').val());
        $('#mob_data2').next('li').text($('#map_get').val());
        $('#mob_data4').next('li').text($('#mob_fio').val());
        $('#mob_data5').next('li').text($('#mob_phone').val());
        $('#mob_data6').next('li').text($('#mob_aditional_info').val());

        $('#www_m_C_ID').val(client_id);
        $('#www_m_S_ID').val(sessind_id);
    });


    // маска для телефона
    $('input[type=tel]').inputmask("+38 (999) 999-99-99");

    // плавная прокрутка
    $('.uk-navbar-nav > li > a').each(function(index, el) {
        $(this).attr('data-uk-smooth-scroll', '{offset: 50}');
        if (index == 1) {
            $(this).attr('data-uk-smooth-scroll', '{offset: 120}');
        }
    });


    // добавляем якоря
    $('.price-block').attr('id', 'prices-and-terms');
    $('.what-need').attr('id', 'how-its-work');
    $('.select-documents').attr('id', 'list-of-documents');

    // добавляем попапы
    $('.callback').attr('data-uk-modal', '{center:true}');



    // работа с geiop

    console.log(city);

    let cityPhone = { // cписок телефонов городов
        'Киев': '+38 (095) 287-9333',
        'Харьков': '+38 (050) 338-9107',
        'Одесса': '+38 (050) 357-7035',
        'Львов': '+38 (050) 390-3924',
        'Днепр': '+38 (050) 390-3918'
    };

    let cityLang = { // перевод городов  
        'Киев': 'Київ',
        'Харьков': 'Харків',
        'Одесса': 'Одеса',
        'Львов': 'Львів',
        'Днепр': 'Дніпро',
        'Ивано-Франковск': 'Івано-Франківськ',
        'Ровно': 'Рівне',
        'Луцк': 'Луцьк',
        'Дубно': 'Дубно',
        'Сарны': 'Сарни'
    };

    {
        for (keyCity in cityLang) {
            if (keyCity != city) {
                city = 'Киев';
            }
        }
    }

    // меняем список адресный сервисов в которых был оформлен паспорт в зависимости от города
    {

        $('[class*=rsform-block-mob-address-get-]').hide();
        $("#select_city option[value="+city+"]").attr('selected', 'selected');
        $('#select_city option').each(function(index, el) {
            if ($(this).attr('selected') == 'selected') {
                $('[class*=rsform-block-mob-address-get-'+ ++index +']').show();
            }
            
        });

    };

    // console.log($('#select_city').val());

    if ($('html').attr('lang') == 'ru-ru') {
        
        for (keyCity in cityLang) {
            if (keyCity == city) {
                $('.lang-city').text(city); // меняем город по тексту
                $('#map_search, #map_get').val(city); // меняем город на карте
                $('.phone-block strong').text(cityPhone[city]); // меняем номер телефона в соотвествии с городом
                $('.city-block a > span').text(city);
                $('.uk-nav-dropdown li').each(function(index, el) {
                    if ($(this).find('a').text() == city) {
                        $(this).addClass('uk-active');
                    }
                });

                if (city != 'Харьков') {

                    UIkit.notify("В городе <strong>" + city + "</strong>, доступен сервис выездного  оформления загранпаспорта. А также ID карты для граждан Украины и вида на жительство для иностранцев. Консультации за телефоном.", { timeout: 30000, status: 'success', pos: 'bottom-right' });
                    break;
                }

                if (city == 'Харьков') {

                UIkit.notify("В городе <strong>" + city + "</strong>, доступен сервис выездного оформления загранпаспорта", { timeout: 5000, status: 'success', pos: 'bottom-right' });
                    break;
                }

             } // else {
            //     UIkit.notify("Сожалеем, в городе <strong>" + city + "</strong> пока не доступен сервис выездного оформления загранпаспорта", { timeout: 0, status: 'danger', pos: 'bottom-right' });
            //     break;
            // }
        }


        $('.city-block .uk-dropdown ul > li > a').click(function(e) {
            let thisname = $(this).text();

            e.preventDefault();
            $('#map_search, #map_get').val(thisname); // меняем город на карте при смене города в ручную
            setTimeout(function() { // обновляем карты
                rsfp_initialize_map90();
                rsfp_initialize_map40();
            }, 100);
            $('.city-block a > span, .lang-city').text(thisname);
            let setClass = $(this).closest('.uk-nav-dropdown');
            setClass.find('li').each(function(index, el) {
                $(this).removeClass('uk-active');
            });
            $(this).closest('li').addClass('uk-active');
            $('.phone-block strong').text(cityPhone[thisname]);
            
            {
                $('[class*=rsform-block-mob-address-get-]').hide();
                $("#select_city option[value="+thisname+"]").attr('selected', 'selected');
                $('#select_city option').each(function(index, el) {
                    if ($(this).attr('selected') == 'selected') {
                        $('[class*=rsform-block-mob-address-get-'+ ++index +']').show();
                    }
                    console.log(index);
                   
                });
            }

        });
    } else if ($('html').attr('lang') == 'uk-ua') {
        

        for (keyCity in cityLang) {
            if (keyCity == city) {
                $('.lang-city').text(cityLang[keyCity]); // меняем город по тексту
                $('#map_search, #map_get').val(cityLang[keyCity]); // меняем город на карте
                $('.phone-block strong').text(cityPhone[city]); // меняем номер телефона в соотвествии с городом
                $('.city-block a > span').text(cityLang[keyCity]);
                $('.uk-nav-dropdown li').each(function(index, el) {
                    if ($(this).find('a').text() == cityLang[keyCity]) {
                        $(this).addClass('uk-active');
                    }
                });

                if (cityLang[keyCity] != 'Харків') {

                    UIkit.notify("В місті <strong>" + cityLang[keyCity] + "</strong>, доступний сервіс виїзного оформлення закордонного паспорта. А також ID карти для громадян України та посвідки на тимчасове проживання для іноземців. Консультації за телефоном.", { timeout: 30000, status: 'success', pos: 'bottom-right' });
                    break;

                }

                if (cityLang[keyCity] == 'Харків') {

                UIkit.notify("В місті <strong>" + cityLang[keyCity] + "</strong>, доступний сервіс виїзного оформлення закордонного паспорта", { timeout: 5000, status: 'success', pos: 'bottom-right' });
                    break;

                }
            } // else {
            //     UIkit.notify("Шкодуємо, в місті <strong>" + city + "</strong> поки не доступний сервіс виїзного оформлення закордонного паспорта", { timeout: 0, status: 'danger', pos: 'bottom-right' });
            //     break;
            // }
        }
        $('.city-block .uk-dropdown ul > li > a').click(function(e) {
            e.preventDefault();
            let thisname = $(this).text();

            $('#map_search, #map_get').val($(this).text()); // меняем город на карте при смене города в ручную
            setTimeout(function() { // обновляем карты
                rsfp_initialize_map90();
                rsfp_initialize_map40();
            }, 100);
            $('.city-block a > span, .lang-city').text($(this).text());
            let setClass = $(this).closest('.uk-nav-dropdown');
            setClass.find('li').each(function(index, el) {
                $(this).removeClass('uk-active');
            });

            $(this).closest('li').addClass('uk-active');
            for (keyCityUa in cityLang) {
                if ($(this).text() == cityLang[keyCityUa]) {
                    $('.phone-block strong').text(cityPhone[keyCityUa]);
                    thisname = keyCityUa;
                    console.log(keyCityUa);
                    break;
                }
            }

             {
                $('[class*=rsform-block-mob-address-get-]').hide();
                $("#select_city option[value="+thisname+"]").attr('selected', 'selected');
                $('#select_city option').each(function(index, el) {
                    if ($(this).attr('selected') == 'selected') {
                        $('[class*=rsform-block-mob-address-get-'+ ++index +']').show();
                    }
                   
                });
            }

            {

                $('#filter-date').val('');
                $('.uk-form.mobile-form .formContainer').each(function(index, el) {
                    $(this).addClass('formHidden');
                    if (index == 0) {
                        $(this).removeClass('formHidden');
                    }
                });
                $('.uk-form.first-form .formContainer').each(function(index, el) {
                    $(this).addClass('formHidden');
                    if (index == 0) {
                        $(this).removeClass('formHidden');
                    }
                });

                $('#next_btn2Next').attr('disabled', 'disable');
                $('#next_btn3Next').attr('disabled', 'disable');

            }

        });
    }

    // показ телефонов в мобильной версии по клику на анимацию телефона

    $('#notify_phone').click(function(event) {
        event.preventDefault();
        console.log();
       if($('.uk-notify.uk-notify-top-center').css('display') == 'none' || $('.uk-notify').hasClass('uk-notify-top-center') == false)  {
            UIkit.notify("<img src='../images/lifecell_ico.png' class='notify-img' alt='phone-lifecell' /> <a href='tel:+380735978777' class='notify-phone'>(073)-597-87-77</a>", { timeout: 5000, status: 'primary', pos: 'top-center' });
            UIkit.notify("<img src='../images/mts_ico.png'  class='notify-img' alt='phone-mts' /> <a href='tel:+380955978777' class='notify-phone'>(095)-597-87-77</a>", { timeout: 5000, status: 'primary', pos: 'top-center' });
            UIkit.notify("<img src='../images/kyivstar_ico.png'  class='notify-img' alt='phone-kyivstar' /> <a href='tel:+380675978777' class='notify-phone'>(067)-597-87-77</a>", { timeout: 5000, status: 'primary', pos: 'top-center' });
            UIkit.notify("<img src='../images/phone-ico.svg'  class='notify-img' alt='phone-icon' /> <a href='tel:+380445978777' class='notify-phone'>(044)-597-87-77 </a>", { timeout: 5000, status: 'primary', pos: 'top-center' });

       }
        
    });


    // блокируем поле ввода даты в ручную

    $('#filter-date').on('keydown', function(event) {
        $('#filter-date').attr('readonly', 'readonly');
        $('#filter-date').datetimepicker('show');
    });

    // добавляем адресс мобильной выдачи в скрытое поле

    $('#mob_next_btn2Next').click(function(event) {
        $('.mob-adress').each(function(index, el) {
            let thisparent = $(this).closest('.rsform-block');
            if (thisparent.css('display') == 'block') {
                $('#hidden_mob_addres').val($(this).val());
            }
        });
    });

    
   

});